import { User } from './../model/user.interface';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupComponent } from './signup.component';
import { NavigationService } from '../service/navigation.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [ SignupComponent ],
      providers: [ NavigationService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('when sign up', () => {
    it('should display error if user is null', () => {
      component.user.password = 'password';
      component.user.email = 'email';
      spyOn<any>(component, 'displayError');
      spyOn(component['signupService'], 'addUser').and.returnValue(null);
      component.onSubmit();
      expect(component['displayError']).toHaveBeenCalledWith('User already exist');
    });
    it('should display error if user email or password is empty', () => {
      component.user.password = '';
      component.user.email = 'email';
      spyOn<any>(component, 'displayError');
      spyOn(component['signupService'], 'addUser').and.returnValue(null);
      component.onSubmit();
      expect(component['displayError']).toHaveBeenCalledWith('email and password are required');
    });
    it('should redirect to news page and set user value to appContext', () => {
      component['appContext'].loginUser = '';
      const user: User = { email: 'tevy' , password: 'ddd' };
      component.user.password = user.password;
      component.user.email = user.email;
      spyOn<any>(component, 'displayError');
      spyOn(component['signupService'], 'addUser').and.returnValue(user);
      spyOn(component['navigationService'], 'goToNewsList');
      component.onSubmit();
      expect(component['displayError']).not.toHaveBeenCalled();
      expect(component['navigationService'].goToNewsList).toHaveBeenCalled();
      expect(component['appContext'].loginUser).toEqual(user.email);
    });
  });
});
