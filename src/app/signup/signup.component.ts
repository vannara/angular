import { AppContextService } from './../service/app-context.service';
import { SingupService } from './../service/singup.service';
import { Component, OnInit } from '@angular/core';
import { User } from '../model/user.interface';
import { NavigationService } from '../service/navigation.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent implements OnInit {
  public errorMessage = '';
  public user: User = { email: '', password: ''};
  constructor(private navigationService: NavigationService,
              private signupService: SingupService,
              private appContext: AppContextService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    let user: User | null = null;
    if (this.user.email !== '' && this.user.password !== '') {
      user = this.signupService.addUser(this.user);
      if (user) {
        this.appContext.loginUser = user.email;
        this.navigationService.goToNewsList();
      } else {
        this.displayError('User already exist');
      }
    } else if (this.user.email === '' || this.user.password === '') {
      this.displayError('email and password are required');
    }
  }

  private displayError(message: string): void {
    this.errorMessage = message;
  }
}
