export class SortHelper {

    /**
     * Sort date by ascending order
     * @param a
     * @param b
     * @returns
     */
    static sortByDateAsc(a: { dateTime: Date }, b: { dateTime: Date }): number {
      if (a.dateTime.getTime() === b.dateTime.getTime()) {
        return 0;
      } else if (a.dateTime.getTime() > b.dateTime.getTime()) {
        return 1;
      } else {
        return -1;
      }
    }

    /**
     * Sort date by descending order
     * @param a
     * @param b
     * @returns
     */
    static sortByDateDesc(a: { dateTime: Date }, b: { dateTime: Date }): number {
      if (a.dateTime.getTime() === b.dateTime.getTime()) {
        return 0;
      } else if (a.dateTime.getTime() > b.dateTime.getTime()) {
        return -1;
      } else {
        return 1;
      }
    }
}
