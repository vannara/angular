import { AppContextService } from './../service/app-context.service';
import { Router } from '@angular/router';
import { SingupService } from './../service/singup.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  public errorMessage = '';
  public email = '';
  public password = '';
  constructor(private signupService: SingupService, private router: Router, private appContext: AppContextService) { }

  ngOnInit(): void {
  }

  /**
   * Navigate to news page if user is already signed up
   * Otherwise show error on the page
   */
  public login(): void {
    const user = this.signupService.getUser(this.email, this.password);
    if (!user) {
      this.errorMessage = 'user name and/or password is not correct';
    } else {
      this.appContext.loginUser = user.email;
      this.router.navigate(['news']);
    }
  }
}
