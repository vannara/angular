import { NavigationService } from './../service/navigation.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppContextService } from '../service/app-context.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(public appContext: AppContextService, public navigationService: NavigationService) { }

  ngOnInit(): void {
    this.navigationService.displayDefaultPage();
  }
}
