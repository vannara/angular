
export interface Item {
    id: number;
    type: string;
    title: string;
    by: string;
    dateTime: Date;
    text: string;
    url: string;
    score: number;
    commentCount: number;
}
