import { NavigationService } from './../service/navigation.service';
import { AppContextService } from './../service/app-context.service';
import { NewsService } from './../service/newslist.service';
import { Component, OnInit } from '@angular/core';
import { Item } from '../model/item.interface';

@Component({
  selector: 'app-new-article',
  templateUrl: './new-article.component.html'
})
export class NewArticleComponent implements OnInit {
  public articleId = '';
  public articleTitle = '';
  public articleText = '';
  public articleScore = '';
  public articleUrl = '';
  public types: string[] = ['job', 'comment', 'story', 'poll'];
  public articleType = '';
  constructor(private newsService: NewsService, private appContext: AppContextService,
              private navigationService: NavigationService) {
  }

  ngOnInit(): void {
    this.articleType = this.types[0];
  }

  public addArticle(): void {
    const item: Item = {
      id: Number(this.articleId),
      title: this.articleTitle,
      text: this.articleText,
      score: Number(this.articleScore),
      url: this.articleUrl,
      type: this.articleType,
      by: this.appContext.loginUser,
      dateTime: new Date(),
      commentCount: 0
    };
    this.newsService.addNewArticle(item);
    this.navigationService.goToNewsList();
  }

  public trackByType(index: number): number {
    return index;
  }

}
