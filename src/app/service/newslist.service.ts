
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Item } from '../model/item.interface';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  public itemIds: number[] = [];
  private newItems: Item[] = [];
  private readonly DEFAULT_PAGE_SIZE = 10;
  private pageSize: number;
  private totalPage = 0;

  constructor(private httpClient: HttpClient) {
    // Read page size from environment setting. If no page size setting, use 10 page size
    this.pageSize = environment.pageSize || this.DEFAULT_PAGE_SIZE;
  }

  public get pageTotal(): number {
    return this.totalPage;
  }

  /**
   * Get item by page number
   * @param pageNumber
   * @returns list of item of the requested page
   */
  public getArticleByPage(pageNumber: number): Observable<Item>[] {
    // The starting index of each page
    const pageOffSet = pageNumber === 1 ? 0 : (this.pageSize * (pageNumber - 1));
    const responses: Observable<Item>[] = [];
    // Push new article to the top
    if (this.newItems.length) {
      for (const newItem of this.newItems) {
        responses.push(of(newItem));
      }
    }
    const sizeToRequest = (pageNumber * this.pageSize) - this.newItems.length;
    this.newItems = [];
    for (let i = pageOffSet; i < sizeToRequest ; i++) {
      responses.push(this.getArticleById(this.itemIds[i]));
    }
    return responses;
  }

  /**
   * Get list of items id
   * @returns a promise of boolean
   */
  public isListLoaded(): Promise<boolean> {
    return new Promise((resolve) => {
      this.getTopStories().subscribe(ids => {
        this.totalPage = Math.ceil(ids.length / this.pageSize);
        this.itemIds = ids;
        resolve(true);
      },
      () => {
        this.itemIds = [];
        resolve(false);
      });
    });
  }

  /**
   * Add new article to the list
   * @param item
   */
  public addNewArticle(item: Item): void {
    // Can be called with valid api
    this.newItems.push(item);
  }

  /**
   * get top stories (also contains jobs)
   * @returns stories id
   */
  public getTopStories(): Observable<number[]> {
    return this.httpClient.get('https://hacker-news.firebaseio.com/v0/topstories.json').pipe(
      map(response => this.parsTopStoriesResponse(response),
      catchError(er => throwError(er))));
  }

  /**
   * Get item by its id
   * @param id
   * @returns
   */
  public getArticleById(id: number): Observable<Item> {
    return this.httpClient.get(`https://hacker-news.firebaseio.com/v0/item/${id}.json`)
      .pipe(map(response =>  this.parseItemResponse(response)));
  }

  private parsTopStoriesResponse(response: any): number[] {
    return response;
  }

  /**
   * Map api model to application model
   * @param response
   * @returns
   */
  private parseItemResponse(response: any): Item {
    const date = new Date(response.time * 1000);
    const item: Item = {
      id: response.id,
      title: response.title,
      score: response.score,
      type: response.type,
      by: response.by,
      dateTime: date,
      text: response.text,
      url: response.url,
      commentCount: Number(response.descendants) || 0
    };
    return item;
  }
}
