import { User } from './../model/user.interface';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SingupService {
  public userList: User[] = [];
  constructor() { }

  /**
   * Add new user
   * @param user
   * @returns null if user exist
   */
  public addUser(user: User): User | null {
    if (this.userList.find(u => u.email === user.email)) {
      return null;
    } else {
      this.userList.push(user);
    }
    return user;
  }

  /**
   * Return existing user
   * @param userName
   * @param password
   * @returns
   */
  public getUser(userName: string, password: string): User | undefined {
    const user = this.userList.find(u => u.email === userName && u.password === password);
    return user;
  }
}
