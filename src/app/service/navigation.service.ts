import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AppContextService } from './app-context.service';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  public readonly LOGIN_MENU = 'login';
  public readonly SIGNUP_MENU = 'signup';
  public readonly NEWS_MENU = 'news';
  public readonly ARTICLE_POST_MENU = 'post-article';

  constructor(private router: Router, private appContext: AppContextService) { }

  public displayDefaultPage(): void {
    if (window.location.href.indexOf(this.LOGIN_MENU) !== -1) {
      this.updateActiveStyle(this.LOGIN_MENU);
    } else if (window.location.href.indexOf(this.NEWS_MENU) !== -1) {
      this.updateActiveStyle(this.NEWS_MENU);
    } else if (window.location.href.indexOf(this.SIGNUP_MENU) !== -1) {
      this.updateActiveStyle(this.SIGNUP_MENU);
    } else if (window.location.href.indexOf(this.ARTICLE_POST_MENU) !== -1) {
      this.updateActiveStyle(this.ARTICLE_POST_MENU);
    }
  }

  public goToLogin(): void {
    this.updateActiveStyle(this.LOGIN_MENU);
    this.router.navigate(['/login']);
  }

  public goToSignUp(): void {
    this.updateActiveStyle(this.SIGNUP_MENU);
    this.router.navigate(['/signup']);
  }

  public goToNewsList(): void {
    this.updateActiveStyle(this.NEWS_MENU);
    this.router.navigate(['/news']);
  }

  public goToPostArticle(): void {
    this.updateActiveStyle(this.ARTICLE_POST_MENU);
    this.router.navigate(['/new-article']);
  }

  public logout(): void {
    this.appContext.loginUser = '';
    this.goToNewsList();
  }

  /**
   * Set new menu to active
   * @param newActiveMenu
   */
  private updateActiveStyle(newActiveMenu: string): void {
    document.getElementsByClassName('active').item(0)?.classList.remove('active');
    document.getElementsByClassName(newActiveMenu).item(0)?.classList.add('active');
  }

}
