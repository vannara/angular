import { NewsListComponent } from './news-list/news-list.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { NewArticleComponent } from './new-article/new-article.component';

const routes: Routes = [
  { path: '', component: NewsListComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'news', component: NewsListComponent },
  { path: 'new-article', component: NewArticleComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
