import { Item } from './../model/item.interface';
import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { NewsService } from '../service/newslist.service';

import { NewsListComponent } from './news-list.component';
import { of } from 'rxjs';

describe('NewsListComponent', () => {
  let component: NewsListComponent;
  let fixture: ComponentFixture<NewsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      declarations: [ NewsListComponent ],
      providers: [ NewsService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('when ids are available', () => {
    let item1: Item;
    let item2: Item;

    beforeEach(() => {
      item1 = {
        id: 1,
        type: 'job',
        title: 'tt',
        by: 'me',
        dateTime: new Date('2021-03-03T12:12:00'),
        text: '',
        url: 'abc.com',
        score: 3,
        commentCount: 0
      };
      item2 = {
        id: 2,
        type: 'story',
        title: 'ss',
        by: 'you',
        dateTime: new Date('2021-03-03T12:15:00'),
        text: '',
        url: 'xxxyyzz.com',
        score: 1,
        commentCount: 0
      };
    });
    it('should get first page on init', done => {
      spyOn(component.newslistService, 'isListLoaded').and.resolveTo(true);
      spyOn<any>(component, 'loadItems');
      component.ngOnInit();
      component.newslistService.isListLoaded().then(() => {
        expect(component['loadItems']).toHaveBeenCalledWith(1);
        done();
      });
    });
    it('should sort item by time', fakeAsync(() => {
      spyOn(component.newslistService, 'getArticleByPage').and.returnValue([of(item1), of(item2)]);
      spyOn<any>(component, 'loadItems').and.callThrough();
      component.loadNextPage();
      component['loadItems'](component.currentPage).subscribe(() => {
        expect(component['loadItems']).toHaveBeenCalledWith(2);
        expect(component.items.length).toEqual(2);
        expect(component.items[0].id).toEqual(2);
        tick();
      });
    }));

    it('should update current page when view more is clicked', () => {
      component.currentPage = 1;
      spyOn<any>(component, 'loadItems').and.returnValue(of([item1]));
      component.loadNextPage();
      expect(component.currentPage).toEqual(2);
    });
  });
});
