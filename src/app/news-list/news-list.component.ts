import { Item } from './../model/item.interface';
import { SortHelper } from './../shared/sort-helper';
import { NewsService } from './../service/newslist.service';
import { Component, OnInit } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {
  // Waiting loader whole page (on first page)
  public isLoading = false;
  // Waiting loader on next page
  public loadingRow = false;
  public items: Item [] = [];
  public currentPage = 1;
  public errorMessage = '';

  constructor(public newslistService: NewsService) { }

  ngOnInit(): void {
    this.isLoading = true;
    // We can only load item once we know their ids
    this.newslistService.isListLoaded().then(value => {
      if (value) {
        this.loadItems(this.currentPage).subscribe(total => {
          this.items = total.sort(SortHelper.sortByDateDesc);
          this.isLoading = false;
        });
      } else {
        this.errorMessage = 'Error while loading list';
      }
    });
  }

  /**
   * When view more is clicked
   */
  public loadNextPage(): void {
    this.loadingRow = true;
    this.currentPage = this.currentPage + 1;
    this.loadItems(this.currentPage).subscribe(total => {
      this.items = this.items.concat(total.sort(SortHelper.sortByDateDesc));
      this.isLoading = false;
      this.loadingRow = false;
    });
  }

  /**
   * Open the article on table row's click
   * @param item
   */
  public gotoArticle(item: Item): void {
    if (item.url !== '') {
      window.location.href = item.url;
    }
  }

  /**
   * Angular performance reason when using for loop
   * @param index
   * @returns
   */
  public trackByArticle(index: number): number {
    return index;
  }

  /**
   * Load Item by page number
   * @param pageNumber
   */
  private loadItems(pageNumber: number): Observable<Item[]> {
    const observableList = this.newslistService.getArticleByPage(pageNumber);
    return forkJoin(observableList);
  }
}
