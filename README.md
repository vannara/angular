# README #

### How do I get set up? ###

* cd angular
* npm install

### Configure page size ###

* go to environment.ts and set page size value

### How Do I Run ###

* npm start
* http://localhost:4200/ is opened

### Unit test ###

* Unit tests are not complete
* but you can run with npm test

### Sign up users ###

* There is no local storage used in the application. As long as you don't refresh the page, the list of sign up users will stay in the session.

### Redirection ###

* After sign up, user is log in automatically 
* and is redirected to list page.

### Sorting List ###

* For performance reason, the request to API is done page by page. Meaning, the latest item on the first page does not necessary mean the last item of all pages.
* I avoid sorting again after loading more item so that the position of the item won't be reordered from first page to second page.
